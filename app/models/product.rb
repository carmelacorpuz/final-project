class Product < ApplicationRecord
    belongs_to :admin
    has_many :order_lines
    
    accepts_nested_attributes_for :order_lines
end
