class Cashier < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :admin
  has_many :orders
  
  def active_for_authentication?
    super and self.status?
  end
  
end
