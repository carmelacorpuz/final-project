class ProductsController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_product, only: [:destroy]
    
    def new
        @product = Product.new
    end
    def destroy
        if @product.status == true
            @product.status = false
        else
            @product.status = true
        end
        if @product.save
            redirect_to products_path
        end
    end
    def activate
        @product = Product.find(params[:id])
        @product.status = true
        @product.save
        redirect_to products_path
    end
    
    def deactivate
        @product = Product.find(params[:id])
        @product.status = false
        @product.save
        redirect_to products_path
    end
    
    def index
        #@products = Product.where(status:true)
        @products = Product.all
    end
    
    def edit
        @product = Product.find(params[:id])
        if @product.admin != current_admin
            redirect_to products_path
        end
    end
    
    def create
        @product = Product.new(product_params)
        @product.status = true
        @product.admin = current_admin
        @product.save
       
       redirect_to @product
    end
    
    def view
        id = params[:id]
        @product = Product.find(id)
    end
    
    def show
        id = params[:id]
        @product = Product.find(id)
    end
    
    def update
        @product = Product.find(params[:id])
        @product.update(product_params)
        @product.save
        redirect_to @product
    end
    
    def report
        @products = Product.all
    end
    
    def cashierreport
        @Cashier = Cashier.find(params[:id])
		#@lines = @Cashier.orderswhere("created_at >= ? AND cashier_id = ?", Time.zone.now.beginning_of_day, @Cashier.id)
		@all_lines = Product.all.where(":created_at >= ? AND :cashier_id = ?", Time.zone.now.beginning_of_day, @Cashier.id)
# 		@all_product = Product.all
	end
    
    private
        def product_params
            params.require(:product).permit(:name, :price, :status)
        end
        def set_product
            @product = Product.find_by(id: params[:id])
            redirect_to products_path, notice: "Product not found." if @product.nil?
        end
end