class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  after_action :store_action_cashier, :store_action_admin
  
  def store_action_cashier
    return unless request.get? 
    if (request.path != "/cashiers/sign_in" &&
        request.path != "/cashiers/password/new" &&
        request.path != "/cashiers/password/edit" &&
        request.path != "/cashiers/confirmation" &&
        request.path != "/cashiers/sign_out" &&
        !request.xhr?) # don't store ajax calls
      store_location_for(:cashier, orders_path)
    end
  end
  
  def store_action_admin
    return unless request.get? 
    if (request.path != "/admins/sign_in" &&
        request.path != "/admins/password/new" &&
        request.path != "/admins/password/edit" &&
        request.path != "/admins/confirmation" &&
        request.path != "/admins/sign_out" &&
        !request.xhr?) # don't store ajax calls
      store_location_for(:admin, products_path)
    end
  end
   
end
