class CashiersController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_cashier, only: [:destroy]
    def destroy
        if @cashier.status == true
            @cashier.status = false
        else
            @cashier.status = true
        end
        if @cashier.save
            redirect_to cashiers_path
        end
    end
    
    def create
        @cashier = Cashier.new(cashier_params)
        @cashier.status = true
        @cashier.admin = current_admin
        @cashier.save
        @cashier_registration = @cashier
        redirect_to @cashier
    end
  
    def new
        @cashier = Cashier.new
    end
    
    def index
        #@cashiers = Cashier.where(status:true)
        @cashiers = Cashier.all
    end
    
    def edit
        @cashier = Cashier.find(params[:id])
        if @cashier.admin != current_admin
            redirect_to cashiers_path
        end
        
    end
    
    def view
        id = params[:id]
        @cashier = Cashier.find(id)
    end
    
    def show
        id = params[:id]
        @cashier = Cashier.find(id)
    end
    
    def update
        @cashier = Cashier.find(params[:id])
        @cashier.update(cashier_params)
        @cashier.save
        redirect_to @cashier
    end
    
    def deactivate
        @cashier = Product.find(params[:id])
        @cashier.status = false
        @cashier.save
        redirect_to cashiers_path
    end
    
    def activate
        @cashier = Product.find(params[:id])
        @cashier.status = true
         @cashier.save
        redirect_to cashiers_path
    end

    private
        def cashier_params
            params.require(:cashier).permit(:email, :password, :password_confirmation, :name, :status)
        end
        def set_cashier
            @cashier = Cashier.find_by(id: params[:id])
            redirect_to cashiers_path, notice: "Cashier not found." if @cashier.nil?
        end
end
