class OrdersController < ApplicationController
    before_action :authenticate_cashier!
    
    def new
        @order = Order.new
    end
    
    def index
        @orders = Order.all
    end
    
    def refund
        @order = Order.find(params[:id])
    end
    
    def update
        @order = Order.find(params[:id])
        @order.update(order_params)
        total = 0
        @order.order_lines.each do |p|
            subtotal = p.product.price*p.quantity
            total = subtotal + total
        end
        @order.total_price = total
        @order.save
        redirect_to @order
    end
    
    def edit
        @order = Order.find(params[:id])
        if @order.cashier != current_cashier
            redirect_to orders_path
        end
    end
    
    def summary
        @order = Order.new(order_params)
        total = 0
        @order.order_lines.each do |p|
            subtotal = p.product.price*p.quantity
            total = subtotal + total
        end
        @order.total_price = total
    end
    
    def cashierreport
 		@cashier = current_cashier
 		#@order.order_lines.each do |p|
 		#    @product = p.product.where(cashier: @cashier)
 		#end
		#@lines = @Cashier.orderswhere("created_at >= ? AND cashier_id = ?", Time.zone.now.beginning_of_day, @Cashier.id)
		#@all_orders = Order.all.where(["created_at >= ? and cashier_id = ?", Time.zone.now.beginning_of_day, Cashier: @Cashier.id])
		#@all_lines = Array.new()
# 		@all = OrderLine.where(cashier: current_cashiers)
		#@all_orders.each do |order|
		# 	@all.each do |line|
		# 		if line.order_id == order.id
		# 			@all_lines.push(line)
		# 		end
		# 	end
		#end
		@order = Order.where(cashier_id:current_cashier)
		@ord = @order.each.order_lines
		raise @ord.inspect
# 		render plain: @all_lines.inspect
	end
	
    def report
        @orders = Order.all
        # @orders = Order.new(order_params)
        # quantity = 0
        # temp_product_id = 0
        # @order.order_lines.each do |p|
        #     if p.order.id == 0
        #         temp_product_id = p.product_id
        #         quantity += p.quantity
        #     else 
        #         if p.product.id == temp_product_id
        #             quantity += p.quantity
        #         end
        #     end
        # end
        # redirect_to orders_path
    end
    
    def create
        @order = Order.new(order_params)
        @order.cashier = current_cashier
        @order.save
        #raise order_params.inspect
        redirect_to @order
    end
    
    def show
        @order = Order.find(params[:id])
        if !@order.present?
            redirect_to order_path
        end
    end
   
    
    def destroy
        @order = Order.find(params[:id])
        @order.destroy
        
        redirect_to orders_path
    end
    
   
    private
        def order_params
            params.require(:order).permit!
        end
end
