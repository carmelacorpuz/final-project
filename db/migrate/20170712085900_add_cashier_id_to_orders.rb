class AddCashierIdToOrders < ActiveRecord::Migration[5.1]
  def change
    add_reference :orders, :cashier, foreign_key: true
  end
end
