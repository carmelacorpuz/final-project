class AddAdminIdToCashier < ActiveRecord::Migration[5.1]
  def change
    add_reference :cashiers, :admin, foreign_key: true
  end
end
