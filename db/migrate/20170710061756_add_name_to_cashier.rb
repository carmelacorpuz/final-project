class AddNameToCashier < ActiveRecord::Migration[5.1]
  def change
    add_column :cashiers, :name, :string
  end
end
