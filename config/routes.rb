Rails.application.routes.draw do
  get 'welcome/index', to: 'welcome#index', as: :welcome_index

  devise_for :cashiers, path: 'cashiers'
  
  devise_for :admins, path: 'admins'
  
  resources :products
  get '/report', to: 'products#report', as: :products_reports
  
  
  resources :cashiers, except: :create
  post 'create_cashier' => 'cashiers#create', as: :create_cashier
  
  resources :orders
  post 'orders/summary', to: 'orders#summary', as: :orders_summary
  get 'cashierreport/:id', to: 'orders#cashierreport', as: :cashierreport
  get 'orders/:id/refund/', to: 'orders#refund', as: :orders_refund
  post 'orders/:id', to: 'orders#update', as: :update_order
  
  resources :cashiers_admin, :controller => 'cashiers'
  

  root :to => 'welcome#index'
end